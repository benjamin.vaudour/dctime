package dctime

import (
	"fmt"
)

//Unit est une unité de durée
type Unit int

//Duration est une durée entre deux indications de temps
type Duration int

const (
	NoUnit Unit = iota
	Minute
	Hour
	Day
	Week
	Month
	Year

	DurationNil Duration = 0
)

const (
	_mask    = ^0
	_maxbits = 8 << (_mask>>8&1 + _mask>>16&1 + _mask>>32&1)

	_nunit  = 3
	_nvalue = _maxbits - _nunit

	_munit = 1<<_nunit - 1
	_vmax  = 1<<(_nvalue-1) - 1
	_vmin  = ^_vmax

	_nday   = 5
	_nmonth = 4
	_nyear  = _nday + _nmonth

	_mday   = 1<<_nday - 1
	_mmonth = 1<<_nmonth - 1
)

var (
	u2b = map[Unit]byte{
		NoUnit: 0,
		Minute: 'M',
		Hour:   'H',
		Day:    'd',
		Week:   'w',
		Month:  'm',
		Year:   'y',
	}

	b2u = func() map[byte]Unit {
		m := make(map[byte]Unit)
		for u, b := range u2b {
			m[b] = u
		}
		return m
	}()

	uclock = map[Unit]bool{
		Minute: true,
		Hour:   true,
	}

	udate = map[Unit]bool{
		Day:   true,
		Week:  true,
		Month: true,
		Year:  true,
	}

	unit = func() map[Unit]bool {
		m := make(map[Unit]bool)
		for u, b := range uclock {
			if b {
				m[u] = b
			}
		}
		for u, b := range udate {
			if b {
				m[u] = b
			}
		}
		return m
	}()
)

func dvalid(v int, u Unit) bool { return unit[u] && v >= _vmin && v <= _vmax }

//NewDuration retourne une durée avec sa
//valeur et son unité spécifiées
func NewDuration(v int, u Unit) Duration {
	if dvalid(v, u) {
		return Duration(int(u) | v<<_nunit)
	}
	return DurationNil
}

//Value Retourne la valeur de la durée
func (d Duration) Value() int { return int(d) >> _nunit }

//Unit retourne l’unité de la durée
func (d Duration) Unit() Unit {
	u := Unit(d & _munit)
	if unit[u] {
		return u
	}
	return NoUnit
}

//IsNil retourne vrai is la durée n’a pas d’unité
func (d Duration) IsNil() bool { return d.Unit() == NoUnit }

//IsClock retourne vrai si la durée a une unité d’heure
func (d Duration) IsClock() bool { return uclock[d.Unit()] }

//IsDate retourne vraie si la durée a une unité de date
func (d Duration) IsDate() bool { return udate[d.Unit()] }

//Inv retourne -d
func (d Duration) Inv() Duration { return NewDuration(-d.Value(), d.Unit()) }

//String retourne la durée sous la forme <valeur><unité>
func (d Duration) String() string {
	u := d.Unit()
	if u == NoUnit {
		return "-"
	}
	return fmt.Sprintf("%d%c", d.Value(), u2b[u])
}
