package dctime

import (
	"fmt"
	"time"
)

//Date représente une date
type Date int

const (
	DateNil Date = 0
)

//IsBissextil retourne vrai si l’année fournie
//en paramètre est bissextile
func IsBissextil(year int) bool { return year%4 == 0 && !(year%100 == 0 && year%400 != 0) }

//EndOfMonth retourne le dernier jour du mois
func EndOfMonth(year, month int) int {
	switch month {
	case 4, 6, 9, 11:
		return 30
	case 2:
		if IsBissextil(year) {
			return 29
		}
		return 28
	}
	return 31
}

//EndOfWeek retourne le nombre de jours
//avant la fin de la semaine
func EndOfWeek(e time.Weekday) int { return (7 - int(e)) % 7 }

func dateValid(year, month, day int) bool {
	return year > 0 && month > 0 && day > 0 && year < 10000 && month < 13 && day <= EndOfMonth(year, month)
}
func date(year, month, day int) Date { return Date(year<<_nyear | month<<_nday | day) }
func t2d(t time.Time) Date {
	year, month, day := t.Date()
	return date(year, int(month), day)
}
func d2t(d Date) time.Time {
	year, month, day := d.Date()
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.Local)
}

//NewDate retourne une date avec l’année, le mois
//et le jour spécifiés
func NewDate(year, month, day int) Date {
	if !dateValid(year, month, day) {
		return DateNil
	}
	return date(year, month, day)
}

//Date retourne l’année, le mois et le jour
func (d Date) Date() (year, month, day int) {
	if d != DateNil {
		dd := int(d)
		year, month, day = dd>>_nyear, _mmonth&(dd>>_nday), _mday&dd
	}
	return
}

//IsNil retourne vrai si la date est invalide
func (d Date) IsNil() bool {
	year, month, day := d.Date()
	return !dateValid(year, month, day)
}

//Formate retourne la date sous la forme YYY<sep1>MM<sep2>JJ
//Si un seul séparateur est donné, sep2 = sep1
func (d Date) Format(seps ...string) string {
	if d.IsNil() {
		return "-"
	}
	year, month, day := d.Date()
	var s1, s2 string
	if len(seps) > 1 {
		s1, s2 = seps[0], seps[1]
	} else if len(seps) > 0 {
		s1, s2 = seps[0], seps[0]
	}
	return fmt.Sprintf("%04d%s%02d%s%02d", year, s1, month, s2, day)
}

//String retourne la date sous la forme YYYY.MM.JJ
func (d Date) String() string { return d.Format(".") }

func (d Date) addDate(y, m, j int) Date { return t2d(d2t(d).AddDate(y, m, j)) }

//Eow retourne la date de fin de semaine
func (d Date) Eow() Date {
	if d.IsNil() {
		return DateNil
	}
	t := d2t(d)
	return t2d(t.AddDate(0, 0, EndOfWeek(t.Weekday())))
}

//Eom retourne la date de fin du mois
func (d Date) Eom() Date {
	if d.IsNil() {
		return DateNil
	}
	year, month, day := d.Date()
	return d.addDate(0, 0, EndOfMonth(year, month)-day)
}

//Add ajoute une durée spécifiée à la date
func (t Date) Add(d Duration) (r Date, ok bool) {
	if ok = !t.IsNil() && d.IsDate(); !ok {
		return
	}
	var dy, dm, dd int
	v := d.Value()
	switch d.Unit() {
	case Day:
		dd = v
	case Week:
		dd = v * 7
	case Month:
		dy, dm = v/12, v%12
		year, month, day := t.Date()
		if e := EndOfMonth(year+dy, month+dm); e < day {
			dd = e - day
		}
	case Year:
		dy = v
		year, month, day := t.Date()
		if e := EndOfMonth(year+dy, month); e < day {
			dd = e - day
		}
	}
	r = t.addDate(dy, dm, dd)
	return
}

//Sub ajoute une durée spécifiée à la date
func (t Date) Sub(d Duration) (r Date, ok bool) { return t.Add(d.Inv()) }

//Diff retourne la durée entre deux dates
func (d1 Date) Diff(d2 Date) Duration {
	if d1.IsNil() || d2.IsNil() {
		return DurationNil
	}
	t1, t2 := d2t(d1), d2t(d2)
	diff := t1.Sub(t2) / time.Hour / 24
	return NewDuration(int(diff), Day)
}

//Compare retourne 1 si d1 après d2,
//-1 si d1 avant d2 et 0 sinon
func (d1 Date) Compare(d2 Date) int {
	if d1.IsNil() {
		if d2.IsNil() {
			return 0
		}
		return 1
	} else if d1 < d2 || d2.IsNil() {
		return -1
	} else if d1 > d2 {
		return 1
	}
	return 0
}

//Now retourne la date et l’heure actuelles
func Now() (d Date, c Clock) {
	t := time.Now()
	year, month, day := t.Date()
	hour, min, _ := t.Clock()
	d = date(year, int(month), day)
	c = NewClock(uint(hour), uint(min))
	return
}

//DateNow retourne la date actuelle
func DateNow() Date {
	d, _ := Now()
	return d
}

//ClockNow retourne l’heure actuelle
func ClockNow() Clock {
	_, c := Now()
	return c
}

//NowIn retourne la date et l’heure actuelles
//incrémentées de la durée spécifiée
func NowIn(dd Duration) (d Date, c Clock) {
	d, c = Now()
	if dd.IsDate() {
		d, _ = d.Add(dd)
	} else if dd.IsClock() {
		vc, u := dd.Value(), dd.Unit()
		if u == Hour {
			vc *= 60
		}
		j, cc := 60*24, int(c)
		vd := vc / j
		vc -= vd * j
		if add := cc + vc; add < int(ClockBegin) {
			vd--
			vc += j
		} else if add > int(ClockEnd) {
			vd++
			vc -= j
		}
		if vd != 0 {
			d, _ = d.Add(NewDuration(vd, Day))
		}
		if vc != 0 {
			c, _ = c.Add(NewDuration(vc, Minute))
		}
	} else {
		d, c = DateNil, ClockNil
	}
	return
}

func DateNowIn(dd Duration) Date {
	d, _ := NowIn(dd)
	return d
}

func ClockNowIn(dd Duration) Clock {
	_, c := NowIn(dd)
	return c
}
