package dctime

import (
	"regexp"
	"strconv"
)

//ParseDate parse une string en date.
//La string doit être sous la forme YYYYMMJJ ou YYYY?MM?JJ
//(? étant un caractère)
func ParseDate(in string) (out Date, ok bool) {
	out = DateNil
	if ok = in == "-"; !ok {
		var year, month, day int
		if ok, _ = regexp.MatchString(`^\d{8}$`, in); ok {
			year, _ = strconv.Atoi(in[:4])
			month, _ = strconv.Atoi(in[4:6])
			day, _ = strconv.Atoi(in[6:])
		} else if ok, _ = regexp.MatchString(`^\d{4}(\.|\/|-)\d{2}(\.|\/|-)\d{2}$`, in); ok {
			year, _ = strconv.Atoi(in[:4])
			month, _ = strconv.Atoi(in[5:7])
			day, _ = strconv.Atoi(in[8:])

		} else if ok, _ = regexp.MatchString(`^\d{2}(\.|\/|-)\d{2}(\.|\/|-)\d{4}$`, in); ok {
			year, _ = strconv.Atoi(in[:6])
			month, _ = strconv.Atoi(in[3:5])
			day, _ = strconv.Atoi(in[:2])
		}
		if ok {
			out = NewDate(year, month, day)
			ok = !out.IsNil()
		}
	}
	return
}

//ParseClock parse une string en heure.
//La string doit être sous la forme hhmm ou hh?mm
//(? étant un caractère)
func ParseClock(in string) (out Clock, ok bool) {
	out = ClockNil
	if ok = in == "-"; !ok {
		var hour, min int
		if ok, _ = regexp.MatchString(`^\d{4}$`, in); ok {
			hour, _ = strconv.Atoi(in[:2])
			min, _ = strconv.Atoi(in[2:])
		} else if ok, _ = regexp.MatchString(`^\d{2}.\d{2}$`, in); ok {
			hour, _ = strconv.Atoi(in[:2])
			min, _ = strconv.Atoi(in[3:])
		}
		out = NewClock(uint(hour), uint(min))
		ok = !out.IsNil()
	}
	return
}

//ParseDuration convertit le paramètre donné
//en durée. Il doit être sous la forme nU où:
//- n est un entier positif ou négatif,
//- U l’unité parmi y, m, w, d, H ou M représentant
//  respectivement l’année, le mois, la semaine,
//  le jour, l’heure et la minute
func ParseDuration(in string) (out Duration, ok bool) {
	out = DurationNil
	if ok = in == "-"; !ok {
		l := len(in) - 1
		if ok = l > 0; ok {
			vin, uin := in[:l], in[l]
			var u Unit
			if u, ok = b2u[uin]; ok {
				v, err := strconv.Atoi(vin)
				if ok = err == nil && v >= _vmin && v <= _vmax; ok {
					out = NewDuration(v, u)
				}
			}
		}
	}
	return
}
