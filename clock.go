package dctime

import (
	"fmt"
)

//Clock représente une heure donnée
type Clock uint

const (
	ClockBegin Clock = 0
	ClockEnd   Clock = 60*24 - 1
	ClockNil         = ^ClockBegin
)

func cvalid(hour, min uint) bool { return hour < 24 && min < 60 }

//NewClocek retourne une heure avec le nombre d’heures
//et de minutes donnés en paramètre
func NewClock(hour, min uint) Clock {
	if !cvalid(hour, min) {
		return ClockNil
	}
	return Clock(60*hour + min)
}

//IsNil vérifie qu’il s’agit d’une heure valide
func (c Clock) IsNil() bool { return c > ClockEnd }

//Clock retourne l’heure et la minute
func (c Clock) Clock() (hour, min uint) {
	if c.IsNil() {
		hour, min = 24, 60
	} else {
		hour, min = uint(c/60), uint(c%60)
	}
	return
}

//Format formate l’heure sous la forme hh<sep>mm
func (c Clock) Format(sep string) string {
	if c.IsNil() {
		return "-"
	}
	hour, min := c.Clock()
	return fmt.Sprintf("%02d%s%02d", hour, sep, min)
}

//String retourne l’heure sous la forme hh:mm
func (c Clock) String() string { return c.Format(":") }

//Diff retourne la durée entre 2 heures
func (c1 Clock) Diff(c2 Clock) Duration {
	if c1.IsNil() || c2.IsNil() {
		return DurationNil
	}
	return NewDuration(int(c1)-int(c2), Minute)
}

//Add ajoute la durée spécifiée à l’heure
func (c Clock) Add(d Duration) (r Clock, ok bool) {
	r = ClockNil
	if c.IsNil() || !d.IsClock() {
		return
	}
	cc, v := int(c), d.Value()
	if d.Unit() == Hour {
		v *= 60
	}
	cc += v
	if ok = cc >= 0 && cc <= int(ClockEnd); ok {
		r = Clock(cc)
	}
	return
}

//Add soustrait la durée spécifiée à l’heure
func (c Clock) Sub(d Duration) (Clock, bool) { return c.Add(d.Inv()) }

//Compare retourne 1 si c1 après c2,
// -1 si c1 avant c2, et 0 sinon.
func (c1 Clock) Compare(c2 Clock) int {
	if c1 < c2 {
		return -1
	} else if c1 > c2 {
		return 1
	}
	return 0
}
